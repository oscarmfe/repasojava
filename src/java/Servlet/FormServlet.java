/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usuario
 */
public class FormServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>FormServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Recogida de datos del Formulario 1</h1>");
            out.println("<h2>Datos leídos</h2>");
            
            String name = request.getParameter("name");                       
            out.println("El nombre es: " + name + "<br>");
            
            String surname = request.getParameter("surname");                       
            out.println("El apellido es: " + surname + "<br>");
            
            Integer age = Integer.parseInt(request.getParameter("age"));                       
            out.println("La edad es: " + age + " años <br>");
            
            String hobbie = request.getParameter("hobbie");                       
            String[] hobbies = request.getParameterValues("hobbies");
            out.println("Lista de hobbies");
            
            out.println("<ul>");
            for(int i=0;i<hobbies.length;i++)
            {
               out.println("<li>"+hobbies[i]+"</li>");
            }
            out.println("</ul>");
            

//            String[] values=request.getParameterValues("hobbies");
//            out.println("Selected Values...");    
//            for(int i=0;i<values.length;i++)
//            {
//               out.println("<li>"+values[i]+"</li>");
//            }
            

//            out.println("Mis hobbies son: " + hobbies[0] + "<br>");
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
