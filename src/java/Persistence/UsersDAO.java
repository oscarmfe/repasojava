/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import Model.Users;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class UsersDAO  extends BaseDAO{
    public ArrayList<Users> getAll(){
        PreparedStatement stmt = null;
        ArrayList<Users> users = null;

        try {
            this.connect();
            stmt = connection.prepareStatement("select * from users");
            ResultSet rs = stmt.executeQuery();
            users = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Users users = new Users();
                users.setId(rs.getInt("id"));
                users.setName(rs.getString("nombre"));
                users.setEmail(rs.getString("email"));
                users.setPassword(rs.getInt("password"));
                users.setEdad(rs.getInt("edad"));

                users.add(users);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return users;
        
    }
    private static final Logger LOG = Logger.getLogger(BookDAO.class.getName());
    
    public void insert(Users users) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO users(id, nombre, email, password, edad)"
                + " VALUES(?, ?, ?, ?, ?)"
        );
        stmt.setString(1, users.getName());
        stmt.setString(2, users.getEmail());
        stmt.setInt(3, users.getPassword());
        stmt.setInt(4, users.getEdad());

        stmt.execute();
        this.disconnect();
    }


    public Users get(int id) throws SQLException {
        LOG.info("get(id)");
        Users users = new Users();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM users"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        LOG.info("consulta hecha");
        if (rs.next()) {
            LOG.info("Datos ...");
            users.setId(rs.getInt("id"));
            users.setName(rs.getString("nombre"));
            users.setEmail(rs.getString("email"));
            users.setPassword(rs.getInt("password"));
            users.setEdad(rs.getInt("edad"));
            LOG.info("Datos cargados");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        this.disconnect();
        return users;
    }
    


    public void update(Users users) throws SQLException {
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "UPDATE books SET "
                        + "nombre = ?, "
                        + "email = ?, "
                        + "password = ?, "
                        + "edad = ? "
                        + " WHERE id = ? "
        );
        stmt.setString(1, users.getName());
        stmt.setString(2, users.getEmail());
        stmt.setInt(3, users.getPassword());
        stmt.setInt(4, users.getEdad());
        stmt.setLong(5, users.getId());
        LOG.info("id : " + users.getId());
        stmt.execute();
        this.disconnect();
        return;
    }    
}
