/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Model.Users;
import Persistence.UsersDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author alumno
 */
public class UsersController extends BaseController {
    
    public void index() {
        LOG.info("UsersController->index()");
        UsersDAO usersDAO;     
        
        //objeto persistencia
        usersDAO = new UsersDAO();
        ArrayList<Users> users = null;

        //leer datos de la persistencia
        synchronized (usersDAO) {
            users = usersDAO.getAll();
        }
        request.setAttribute("usersList", users);
        String name = "index";
        LOG.info("En ModuleController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/users/index.jsp");        
    }
    
    public void show(String idString) throws SQLException {
        LOG.info("UsersController->show(" + idString +")");
        Users users;
        //objeto persistencia
        UsersDAO usersDAO;             
        usersDAO = new UsersDAO();
        users = usersDAO.get(Integer.parseInt(idString));        
        request.setAttribute("users", users);
        dispatch("/WEB-INF/users/show.jsp");        
    }
    public void create() {
        dispatch("/WEB-INF/users/create.jsp");        
        LOG.info("UsersController->create");
    }
    public void store() throws SQLException {
        //crear objeto
        Users users = loadFromRequest();

//        Book book = new Book();
//        book.setTitle(request.getParameter("title"));
//        book.setPublisher(request.getParameter("publisher"));
//        book.setPages(Integer.parseInt(request.getParameter("pages")));
//        book.setYear(Integer.parseInt(request.getParameter("year")));
        
        //objeto persistencia
        UsersDAO usersDAO;             
        usersDAO = new UsersDAO();
        
        //guardar objeto
        usersDAO.insert(users);
        LOG.info("UsersController->create");
        
        //redirigir
        redirect(contextPath + "/users/index");
        
    }


    public void edit(String idString) throws SQLException {
        LOG.info("idString");
        long id = toId(idString);
        UsersDAO  usersDAO = new UsersDAO();
        Users users = usersDAO.get((int) id);
        request.setAttribute("users", users);
        dispatch("/WEB-INF/users/edit.jsp");

    }

    public void update() throws IOException, SQLException {
        UsersDAO  usersDAO = new UsersDAO();
        Users users = loadFromRequest();
        //crear objeto
        users.setId(Integer.parseInt(request.getParameter("id")));
        
        usersDAO.update(users);
        response.sendRedirect(contextPath + "/users/index");
        return;
    }

    private Users loadFromRequest()
    {
        Users users = new Users();
        users.setName(request.getParameter("nombre"));
        users.setEmail(request.getParameter("email"));
        users.setPassword(Integer.parseInt(request.getParameter("password")));
        users.setEdad(Integer.parseInt(request.getParameter("edad")));
        return users;
    }
            
            

    private static final Logger LOG = Logger.getLogger(BookController.class.getName());

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
