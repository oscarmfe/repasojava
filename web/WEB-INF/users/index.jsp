<%-- 
    Document   : index
    Created on : 05-jun-2017, 19:52:38
    Author     : usuario
--%>

<%@page import="java.util.Iterator"%>
<%@page import="Model.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:useBean id="usersList" scope="request" class="java.util.ArrayList" />

    </head>
    <body>
        <h1>Lista de Alumnos</h1>
        <p><a href="<%= request.getContextPath()%>/users/create">Nuevo</a></p>

        <table>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Password</th>
                <th>Edad</th>
            </tr>
            <%
                Iterator<Model.Users> iterator = usersList.iterator();
                while (iterator.hasNext()) {
                    Users item = iterator.next();%>
            <tr>
                <td><%= item.getId()%></td>
                <td><%= item.getName()%></td>
                <td><%= item.getEmail()%></td>
                <td><%= item.getPassword()%></td>
                <td><%= item.getEdad()%></td>
                <td>
                    <a href="<%= request.getContextPath()%>/users/register/<%= item.getId() %>">Registrar</a> 
                    <a href="<%= request.getContextPath()%>/users/edit/<%= item.getId() %>">Editar</a> 
                </td>
            </tr>
            <%
                }
            %>        
        </table>

    </body>
</html>
