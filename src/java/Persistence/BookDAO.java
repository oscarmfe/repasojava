/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import Model.Book;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class BookDAO  extends BaseDAO{
    public ArrayList<Book> getAll(){
        PreparedStatement stmt = null;
        ArrayList<Book> books = null;

        try {
            this.connect();
            stmt = connection.prepareStatement("select * from books");
            ResultSet rs = stmt.executeQuery();
            books = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setTitle(rs.getString("title"));
                book.setPublisher(rs.getString("publisher"));
                book.setPages(rs.getInt("pages"));
                book.setYear(rs.getInt("year"));

                books.add(book);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return books;
        
    }
    private static final Logger LOG = Logger.getLogger(BookDAO.class.getName());
    
    public void insert(Book book) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO books(title, publisher, year, pages)"
                + " VALUES(?, ?, ?, ?)"
        );
        stmt.setString(1, book.getTitle());
        stmt.setString(2, book.getPublisher());
        stmt.setInt(3, book.getYear());
        stmt.setInt(4, book.getPages());

        stmt.execute();
        this.disconnect();
    }


    public Book get(int id) throws SQLException {
        LOG.info("get(id)");
        Book book = new Book();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM books"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        LOG.info("consulta hecha");
        if (rs.next()) {
            LOG.info("Datos ...");
            book.setId(rs.getInt("id"));
            book.setTitle(rs.getString("title"));
            book.setPublisher(rs.getString("publisher"));
            book.setYear(rs.getInt("year"));
            book.setPages(rs.getInt("pages"));
            LOG.info("Datos cargados");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        this.disconnect();
        return book;
    }
    

    public void delete(int id) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "DELETE FROM books"
                + " WHERE id = ?"
        );
        stmt.setInt(1, id);
        stmt.execute();
        this.disconnect();
    }    
    
    public void update(Book book) throws SQLException {
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "UPDATE books SET "
                        + "title = ?, "
                        + "publisher = ?, "
                        + "year = ?, "
                        + "pages = ? "
                        + " WHERE id = ? "
        );
        stmt.setString(1, book.getTitle());
        stmt.setString(2, book.getPublisher());
        stmt.setInt(3, book.getYear());
        stmt.setInt(4, book.getPages());
        stmt.setLong(5, book.getId());
        LOG.info("id : " + book.getId());
        stmt.execute();
        this.disconnect();
        return;
    }    
}
