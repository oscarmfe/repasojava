CREATE TABLE users2 (
    id INT NOT NULL AUTO_INCREMENT ,
    name VARCHAR(255) NOT NULL UNIQUE, 
    email VARCHAR(255) NOT NULL, 
    password VARCHAR(255), 
    age INT ,
    PRIMARY KEY (id)) ENGINE = InnoDB;


# 1. Alta de alumno: /users/register y /users/store          2 Puntos
# 2. Lista de alumnos: /users                                2 Puntos
# 3. Modificar el perfil del propio alumno:                  2 Puntos
#    /users/edit/{id}
#    /users/update
# 4. Login: name + password                                  2 Puntos
#    /users/login: muestra formulario
#    /users/run:   comprueba credenciales y guarda en sesión
# 5. Modifica las vistas anteriores para que aparezca el usuario logueado
                                                            2 Puntos